package nl.utwente.di.Fahrenheit;
import org.junit.jupiter.api.Assertions;
import org. junit . jupiter .api.Test;
public class TestQuoter {
    @Test
    public void testTemperature1 ( ) throws Exception {
        Quoter quoter = new Quoter();
        double price = quoter.getFahrenheit("25");
        Assertions.assertEquals(77, price,0.0,"Converted temperature 1");
    }
}
