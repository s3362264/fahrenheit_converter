package nl.utwente.di.Fahrenheit;


import java.util.HashMap;
import java.util.Map;

public class Quoter {



    // Implementation of getBookPrice method
    public double getFahrenheit(String celsius) {
        // Check if the celsius exists in the map, if not, return 0.0
        return Double.parseDouble(celsius)* 9 / 5 + 32;
    }
}
